import React from 'react';
import uuid from 'uuid'

class AddProject extends React.Component {
  constructor() {
    super()
    this.state = {
      newProject:{}
    }
  }
  static defaultProps = {
    categories: ['Web design', 'Web Development', 'Mobile Development']
  }

  handleSubmit(e){
    if (this.refs.title.value === '') {
      alert('Title is required')
    } else {
      this.setState({newProject:{
        id: uuid.v4(),
        title: this.refs.title.value,
        category: this.refs.category.value
      }}, function () {
        // console.log(this.state)
        this.props.addProject(this.state.newProject)
      });
    }
    e.preventDefault();
    console.log(this.refs.title.value)
  }

  render(){

    let categoryOptions = this.props.categories.map(category => {
      return <option key={category} value={ category }>{category}</option>
    })

    return(
      <div className="AddProject">
        <h3>AddProject Component</h3>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <div>
            <label>Title</label> <br/>
            <input type="text" ref="title"/>
          </div>
          <div>
            <label>Category</label> <br/>
            <select ref="category">
              {categoryOptions}
            </select>
          </div>
          <input type="submit" value="Submit"/>
        </form>
      </div>
    );
  }
}

export default AddProject;
